import React from "react";
import { slide as Menu } from "react-burger-menu";


export default props => {
  return (
    // Pass on our props
    <Menu {...props}>

      <a className="menu-item" href="/">
        Home
      </a>

      <a className="menu-item" href="/aboutme">
        Sobre mim
      </a>

      <a className="menu-item" href="/follow">
        Fica por dentro
      </a>

      <a className="menu-item" href="/contact">
        Contato
      </a>
    </Menu>
  );
};



      // <Link to="/" className="menu-item">
      //  Home    
      // </Link>
      
      // <Link to="/aboutme" className="menu-item">
      //  Sobre mim      
      // </Link>
  
      // <Link to="/follow" className="menu-item">
      //  Fica por Dentro
      // </Link>
      
      // <Link to="/contact" className="menu-item">
      //  Contato
      // </Link>
      
