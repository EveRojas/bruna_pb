import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from "@material-ui/core/Button";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Profile from "../assets/profile.jpg";
import MenuListComposition from './togglemenu.js'


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '10px',  
    width: '100%', 
    height: '100%',
  },
  media: {
    height: '110px',
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
    font:'400 18px system-ui',
  },
  action: {
    font:'400 18px system-ui',
  },
    actionsub: {
    font:'400 12px system-ui',
  },
}));

export default function AboutMe() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  return (
<div id='page-wrap2'>
  <div className="container">  
    <Card className={classes.root}>
      <CardHeader className={classes.action}
        avatar={
          <Avatar aria-label="profile" className={classes.avatar}>
            BG
          </Avatar>
        }
        action={
          <IconButton aria-label="settings" className={classes.action}>
            
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreVertIcon />
      </Button>
      <Menu
        className={classes.actionsub}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem className={classes.actionsub} onClick={handleClose}>
        <IconButton aria-label="add to favorites" 
                    className={classes.actionsub} 
                    href="https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20">          
                    Consultoria
        </IconButton>
        </MenuItem>
        <MenuItem className={classes.actionsub} onClick={handleClose}>
        <IconButton aria-label="add to favorites" 
                    className={classes.actionsub} 
                    href="https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20">          
                    Private Chef
        </IconButton>
        </MenuItem>
        <MenuItem className={classes.actionsub} onClick={handleClose}>
        <IconButton aria-label="add to favorites" 
                    className={classes.actionsub} 
                    href="follow">          
                    Saiba Mais
        </IconButton>
        </MenuItem>
        </Menu>
          </IconButton>
  
        }
        title="Cozinha do Mundo"
        subheader="Sabores Locais"
      />
      <CardMedia
        className={classes.media}
        image={Profile}
        title="Chef Bruna Grimaldi"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p" style={{font:'400 12px system-ui'}}>
          Uma Chef que traz consigo os temperos do mundo harmonizando-os com os sabores e saberes locais. 
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites" href="https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20">
          <WhatsAppIcon
          />
        </IconButton>
        <IconButton aria-label="share" href="whatsapp://send?text=www.brunagrimaldi.com" data-action="share/whatsapp/share">
          <ShareIcon/>
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent >
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}>
           Foi em Recife que Bruna começou sua trajetória de cozinha profissional. 
           Tem formação acadêmica no Brasil e na Itália. Além do Brasil, trabalhou em  Angola, Califórnia, Canadá, Itália e Londres. 
           Com essa bagagem de diferentes culturas, nossa Chef entende que o que colocamos no prato fala muito sobre nossa cultura, 
           quem nós somos e como lidamos com o mundo ao nosso redor. 
          </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}>
            Diante dos acontecimentos dos últimos anos, envolvendo o meio ambiente, a Chef Bruna Grimaldi compreende que é hora de tomar uma atitude diferente.
            Ela defende o consumo consciente e a preocupação com a cadeia produtiva, dando prioridade a produtores locais.
          </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}> 
           Nossa querida Chef acredita que é nosso dever, como amantes do universo gastronômico, encorajar, dar suporte e agir em favor da sustentabilidade. 
          </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}> 
           A gente te convida a provar o trabalho dessa Chef maravilhosa para experimentar uma diversidade de pratos deliciosos com menos desperdício, menos impacto ambiental e com menor utilização de produtos animais.       
           </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}>
           Sem radicalizar, vamos juntos ampliar nossa consciência e esbanjar amor com ações simples.
          </Typography>
                  <div className="grid-2">
        <Button variant="outlined" color="secondary" href="contact" style={{backgroundColor:'transparent',
    color:'red', font:'400 12px system-ui'}}>Consultoria</Button>
        <Button variant="outlined" color="secondary" href="https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20" 
         style={{backgroundColor:'transparent',
    color:'red', font:'400 12px system-ui'}}>Private Chef</Button>
        </div>


        </CardContent>
      </Collapse>
    </Card>
  </div>
</div>
  );
}