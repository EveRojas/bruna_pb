import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import { createMuiTheme, ThemeProvider,  } from "@material-ui/core/styles";
import { Fade } from '@material-ui/core';
import Button from "@material-ui/core/Button";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import Getin from '../assets/bluebird.jpg';
import Getin2 from '../assets/bottura1.jpg'
import Getin3 from '../assets/carre.jpg'
import Getin4 from '../assets/osteriaflag.jpg'
import Getin5 from '../assets/small.jpg'
import Getin6 from '../assets/parmegiano.jpg'
import Getin7 from '../assets/eletric.jpg'
import Getin8 from '../assets/angola.jpg';
import Getin9 from '../assets/toronto.jpg';
import SocialFollow from './socialfollow.js'
import Divider from '@material-ui/core/Divider';

const theme = createMuiTheme({
  props: {
    // Name of the component ⚛️
    MuiButtonBase: {
      // The default props to change
      ableRipple: true // No more ripple, on the whole application 💣!
    }
  }
});
  
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '10px', 
},

  divider: {
    margin: theme.spacing(0, 0),
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    backgroundColor:'#f0f0f0',
    color: theme.palette.text.secondary,
  },

  MuiGridContainer:{
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
  },

}));

export default function FollowEn() {
  const classes = useStyles();
   function FormRow() {
    return (
      <React.Fragment>
        <Grid item xs={4}>
          <Paper className={classes.paper}>item</Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>item</Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper}>item</Paper>
        </Grid>
      </React.Fragment>
    );
  }

  return ( 
 <div id='page-wrap6'>
      <Grid container>
        <Grid item  xs={2} style={{background:'transparent', marginRight:'3rem'}}>
                   <Fade in timeout={2000}><img style={{width:'95%', height:'65%',  boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '5px',}} src={Getin2} alt='bottura'/></Fade>
              <Button  variant="outlined" color="secondary" href="https://www.instagram.com/bruna_grimaldi/?hl=pt" style={{backgroundColor:'transparent',
    color:'white', font:'400 11px system-ui', display:'flex',width:'95%',
  justifyContent: 'space-around', alignItems:'center', textAlign:'center'}}>FOLLOW</Button>

        </Grid>
        <Grid item xs={2} style={{background:'black',  marginRight:'3rem'}}>
                   <Fade in timeout={3000}><img style={{width:'95%', height:'70%',  boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '5px',}} src={Getin} alt='bluebird'/></Fade>
                   <Button  variant="outlined" color="secondary" href="" style={{backgroundColor:'transparent',
    color:'white', font:'400 11px system-ui', display:'flex',width:'95%',
  justifyContent: 'space-around', alignItems:'center', textAlign:'center'}}>RECIPES</Button>
   
  
        </Grid>
          
        <Grid item xs={2} style={{background:'black',  marginRight:'3rem'}}>
                   <Fade in timeout={4000}><img style={{width:'95%', height:'75%',  boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '5px',}} src={Getin7} alt='eletric' /></Fade>
                           <Button  variant="outlined" color="secondary" href="contactEn" style={{backgroundColor:'transparent',
    color:'white', font:'400 11px system-ui', display:'flex',width:'95%',
  justifyContent: 'space-around', alignItems:'center', textAlign:'center'}}>CLASSES</Button>
   
        </Grid>
        <Grid item xs={2} style={{background:'black',  marginRight:'3rem'}}>
                   <Fade in timeout={5000}><img style={{width:'95%', height:'80%',  boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '5px',}} src={Getin6} alt='parmigiano'/></Fade>
                <Button  variant="outlined" color="secondary" href="" style={{backgroundColor:'transparent',
    color:'white', font:'400 11px system-ui', display:'flex',width:'95%',
  justifyContent: 'space-around', alignItems:'center', textAlign:'center'
}}>TIPS</Button>
        </Grid>
                <Grid item xs={2} style={{background:'black'}}>
                   <Fade in timeout={5000}><img style={{width:'95%', height:'84%',  boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '5px',}} src={Getin5} alt='small'/></Fade>           
        </Grid>
           
      </Grid>
    </div>
  );
}

