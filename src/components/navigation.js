import React from 'react';
import { Link } from 'react-router-dom';
 
 
const Navigation = () => (
  <div>
    <ul>
      <li>
        <Link to='/'>Home</Link>
      </li>
      <li>
        <Link to='/aboutme'>Sobre mim</Link>
      </li>
      <li>
        <Link to='/follow'>Fica por dentro</Link>
      </li>
      <li>
        <Link to='/contact'>Contato</Link>
      </li>
    </ul>
  </div>
);
 
export default Navigation;