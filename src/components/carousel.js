import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ImageGallery from 'react-image-gallery';
import Cracker from "../assets/kalecracker.jpeg";
import Homus from "../assets/homus.jpg";
import Cleanser from "../assets/cleanser1.jpg";
import Carre from "../assets/carre.jpg";
import Lobster from "../assets/lobster.jpg";
import Leeks from "../assets/leeksconfit.jpeg";
import Rosbife from "../assets/rosbife.jpeg";
import Tacos from "../assets/prawntaco.jpeg";
import Cheesebread from "../assets/cheesebread.jpeg";
import Dessert from "../assets/dessert.jpg";



const images = [
  {original: Cracker, width: 960, height: 300},
  {original: Lobster, width: 960, height: 300},
  {original: Homus, width: 960, height: 300},
  {original: Tacos, width: 960, height: 300},
  {original: Rosbife, width: 960, height: 300},
  {original: Carre, width: 960, height: 300},
  {original: Cleanser, width: 960, height: 300},
  {original: Leeks, width: 960, height: 300},
  {original: Cheesebread, width: 960, height: 300},
  {original: Dessert, width: 960, height: 300},
];

class Carousel extends React.Component {
  render() {
    return <ImageGallery items={images} />;
  }
}

export default Carousel;

