import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Profile2 from "../assets/profile2.jpg"
import Brand from "../assets/brand.jpg";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '10px',  
    width: '100%', 
    height: '100%',
     },
  media: {
    height: 300,
  },
});

export default function MediaEn() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={Profile2}
          title="Chef Bruna Grimaldi"
          href="https://api.whatsapp.com/send?phone=447379827042&text=Hi%20there!%20I'm%20the%20Chef%20Bruna%20Grimaldi.%20Nice%20to%20have%20you%20here.How%20can%20I%20help%20you?"
        />
        <CardContent>
          <Typography gutterBottom style={{font:'400 18px system-ui', paddingBottom:'0.5rem'}}>
              Let's work together?
          </Typography>
          <Typography gutterBottom style={{font:'400 13px system-ui'}}>
            Consultancy <img src={Brand} style={{width:'13px', height:'13px', marginTop:'1.2rem', paddingRight:'0.1rem'}}/>
 Events <img src={Brand} style={{width:'13px', height:'13px', marginTop:'1.2rem', paddingRight:'0.1rem'}}/> 
 Classes <img src={Brand} style={{width:'13px', height:'13px', marginTop:'1.2rem', paddingRight:'0.1rem'}}/> 
 Private Chef
          </Typography>
          <Typography gutterBottom color="textSecondary" style={{font:'400 10px system-ui', paddingTop:'0.5rem', paddingBottom:'0.5rem'}}>
            brunagrimaldi@hotmail.com
          </Typography>
          <Typography variant="body2" color="textSecondary" style={{font:'400 10px system-ui'}}>
            +44 7379 827042
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="secondary" style={{font:'400 13px system-ui'}} href="https://api.whatsapp.com/send?phone=447379827042&text=Hi%20there!%20I'm%20the%20Chef%20Bruna%20Grimaldi.%20Nice%20to%20have%20you%20here.How%20can%20I%20help%20you?">
          WHATSAPP
        </Button>
        <Button size="small" color="secondary" href="knowmore" style={{font:'400 13px system-ui'}}>
          Know More
        </Button>
      </CardActions>
    </Card>
  );
}