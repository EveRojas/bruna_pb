import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from "@material-ui/core/Button";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Profile from "../assets/profile.jpg";
import MenuListComposition from './togglemenu.js'


const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '10px',  
    width: '100%', 
    height: '100%',
  },
  media: {
    height: '110px',
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
    font:'400 18px system-ui',
  },
  action: {
    font:'400 18px system-ui',
  },
    actionsub: {
    font:'400 12px system-ui',
  },
}));

export default function KnowMore() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  return (
<div id='page-wrap2'>
  <div className="container">  
    <Card className={classes.root}>
      <CardHeader className={classes.action}
        avatar={
          <Avatar aria-label="profile" className={classes.avatar}>
            BG
          </Avatar>
        }
        action={
          <IconButton aria-label="settings" className={classes.action}>
            
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreVertIcon />
      </Button>
      <Menu
        className={classes.actionsub}
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem className={classes.actionsub} onClick={handleClose}>
        <IconButton aria-label="add to favorites" 
                    className={classes.actionsub} 
                    href="https://api.whatsapp.com/send?phone=447379827042&text=Hi%20there!%20I'm%20the%20Chef%20Bruna%20Grimaldi.%20Nice%20to%20have%20you%20here.How%20can%20I%20help%20you?">          
                    Consultancy
        </IconButton>
        </MenuItem>
        <MenuItem className={classes.actionsub} onClick={handleClose}>
        <IconButton aria-label="add to favorites" 
                    className={classes.actionsub} 
                    href="https://api.whatsapp.com/send?phone=447379827042&text=Hi%20there!%20I'm%20the%20Chef%20Bruna%20Grimaldi.%20Nice%20to%20have%20you%20here.How%20can%20I%20help%20you?">          
                    Private Chef
        </IconButton>
        </MenuItem>
        <MenuItem className={classes.actionsub} onClick={handleClose}>
        <IconButton aria-label="add to favorites" 
                    className={classes.actionsub} 
                    href="followEn">          
                    Get to know
        </IconButton>
        </MenuItem>
        </Menu>
          </IconButton>
  
        }
        title="World Kitchen"
        subheader="Local Flavours"
      />
      <CardMedia
        className={classes.media}
        image={Profile}
        title="Chef Bruna Grimaldi"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p" style={{font:'400 12px system-ui'}}>
          A Chef that brings with herself the spices of the world harmonizing them with the local flavours and culture.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites" href="https://api.whatsapp.com/send?phone=447379827042&text=Hi%20there!%20I'm%20the%20Chef%20Bruna%20Grimaldi.%20Nice%20to%20have%20you%20here.How%20can%20I%20help%20you?">
          <WhatsAppIcon
          />
        </IconButton>
        <IconButton aria-label="share" href="whatsapp://send?text=www.brunagrimaldi.com" data-action="share/whatsapp/share">
          <ShareIcon/>
        </IconButton>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent >
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}>
          Bruna started her career as a professional cook chef in Recife.  
           Her academic background comes from Brazil and Italy. 
           Bruna worked in Brazil, Angola, United States, Italy and UK. 
           Therefore through this multicultural training our Chef understands that what we put on the plate speaks loudly about our culture, who we are and how we deal with the world around us.
          </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}>
            Given the events of the past few years, concerning the environment, our Chef comprehends that is time to take an different attitude towards it. 
           She defends a conscious consumption and is concern about the production chain.
          </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}> 
           Our dear Chef, Bruna Grimaldi, believes it is our duty, as lovers of the gastronomic universe, to encourage, to support and to act on the sustainability behalf.
          </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}> 
            We want to invite you to tasty our Chef work to understand that you can have delicious meals with less waste, less environmental impact and less use of animal products.        
           </Typography>
          <Typography paragraph style={{font:'300 14px system-ui', lineHeight:'1.5'}}>
             Without radicalizing, we will expand our awareness and spread love with simple actions.
          </Typography>
                  <div className="grid-2">
        <Button variant="outlined" color="secondary" href="contactEn" style={{backgroundColor:'transparent',
    color:'red', font:'400 12px system-ui'}}>Consultancy</Button>
        <Button variant="outlined" color="secondary" href="https://api.whatsapp.com/send?phone=447379827042&text=Hi%20there!%20I'm%20the%20Chef%20Bruna%20Grimaldi.%20Nice%20to%20have%20you%20here.How%20can%20I%20help%20you?" 
         style={{backgroundColor:'transparent',
    color:'red', font:'400 12px system-ui'}}>Private Chef</Button>
        </div>


        </CardContent>
      </Collapse>
    </Card>
  </div>
</div>
  );
}