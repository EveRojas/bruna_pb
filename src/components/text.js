import React from "react";
import ReactDOM from "react-dom";

function textEnglish() {

  return (
    <div id="App">
       <div id="pagewrap">
        <text> 
           <i>Bruna started her career as a professional cook chef in Recife.  
           Her academic background comes from Brasil and Italy. 
           Bruna worked on Brasil, Angola, United States, Italy and UK. 
           Therefore through this multicultural training our Chef understands that what we put on the plate speaks loudly about our culture, who we are and how we deal with the world around us. 
           Given the events of the past few years, concerning the environment, our Chef comprehends that is time to take an different attitude towards it. 
           She defends a conscious consumption and is concern about the production chain. 
           Our Chef, Bruna Grimaldi, believes it is our duty as lovers of the gastronomic universe to encourage, to support and to act on the sustainability behalf. 
           We want to invite you to tasty our Chef work to understand that you can have delicious meals with less waste, less environmental impact and less use of animal products. 
           Without radicalizing, we will expand our awareness and spread love with simple actions.</i>
        </text>
      </div>
    </div>
  );
}

export default textEnglish;