import React from "react";
//import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebook, faWhatsapp,faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { makeStyles} from '@material-ui/core/styles';
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { Fade } from '@material-ui/core';

export default function SocialFollow(){
  return(
    <Fade in timeout={2000}><div className='social-container'>
    <a href='https://www.facebook.com/brunaperrelli'
       className="social-icons">
       <FontAwesomeIcon icon={faFacebook} size="1x"/>
    </a>
    <a href='https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20'
       className="social-icons">
       <FontAwesomeIcon icon={faWhatsapp} size="1x"/>
    </a>
        <a href='https://www.instagram.com/bruna_grimaldi'
       className="social-icons">
       <FontAwesomeIcon icon={faInstagram} size="1x"/>
    </a>
    </div></Fade>
  );
}