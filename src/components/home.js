import React from "react";
import ReactDOM from "react-dom";
import Carousel from "./carousel";

function Home() {
  return (
    <div id="App">
       <div className="carousel">
        <Carousel/>
      </div>
    </div>
  );
}

export default Home;