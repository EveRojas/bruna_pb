import React, {useState} from "react";
import ReactDOM from "react-dom";
import textEnglish from './text.js';


function aboutTest (props) {
  const [isActive, setActive] = useState(false);

  const toggleClass = () => {
    setActive(!isActive);
  };

  return (
    <div 
      className={isActive ? 'text': null} 
      onClick={toggleClass} 
    >
      <textEnglish/>
    </div>
   );
}
export default aboutTest;