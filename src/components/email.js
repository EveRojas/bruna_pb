import React, { Component } from 'react'
import * as emailjs from 'emailjs-com'
import { makeStyles} from '@material-ui/core/styles';
import { createMuiTheme, MuiThemeProvider, ThemeProvider } from "@material-ui/core/styles";
import { Fade, Input, FormControl, FormGroup, FormLabel } from '@material-ui/core';
import Button from "@material-ui/core/Button";


class Email extends Component {

  state = {
    name: '',
    email: '',
    subject: '',
    message: '',
  }


handleSubmit(e) {
    e.preventDefault()
    const { name, email, subject, message } = this.state
    let templateParams = {
      from_name: email,
      to_name: 'Chef Bruna Grimaldi',
      subject: subject,
      message_html: message,
     }
     emailjs.send(
      'gmail',
      'template_xQXMnvwP',
       templateParams,
      ' user_iCMAAFwIGApXuYA4oZ63K'
     )
     this.resetForm()
 }
resetForm() {
    this.setState({
      name: '',
      email: '',
      subject: '',
      message: '',
    })
  }
handleChange = (param, e) => {
    this.setState({ [param]: e.target.value })
  }

render() {
    return (
      <>
        <form className='form'>
          <h2>Entra em contato!</h2>
          <FormControl onSubmit={this.handleSubmit.bind(this)}>
            <FormGroup>
              <FormLabel color="secondary" style={{font:'400 15px system-ui'}}>Email:</FormLabel>
              <Input
                color="secondary"
                type="email"
                name="email"
                value={this.state.email}
                style={{marginBottom:'1rem'}}
                onChange={this.handleChange.bind(this, 'email')}
                placeholder="Enter email"
              />
            </FormGroup>
<FormGroup controlId="name" >
              <FormLabel color="secondary"  style={{font:'400 15px system-ui'}}>Nome:</FormLabel>
              <Input 
                color="secondary"
                type="text"
                name="name"
                value={this.state.name}
                style={{marginBottom:'1rem'}}
                onChange={this.handleChange.bind(this, 'name')}
                placeholder="Name"
              />
            </FormGroup>
<FormGroup controlId="formBasicSubject">
              <FormLabel color="secondary"  style={{font:'400 15px system-ui'}}>Assunto:</FormLabel>
              <Input
                color="secondary"
                type="text"
                name="subject"
                style={{marginBottom:'1rem'}}
                value={this.state.subject}
                onChange={this.handleChange.bind(this, 'subject')}
                placeholder="Subject"
              />
            </FormGroup>
<FormGroup controlId="formBasicMessage">
              <FormLabel color="secondary"  style={{font:'400 15px system-ui'}}>Mensagem:</FormLabel>
              <Input
                color="secondary"
                type="textarea"
                name="message"
                style={{marginBottom:'1rem'}}
                value={this.state.message}
                placeholder='Write a message'
                onChange={this.handleChange.bind(this, 'message')}
              />
              <Button variant="primary" type="submit" style={{backgroundColor:'black',
               color:'white', font:'400 15px system-ui', marginTop:'0rem', marginBottom:'1rem'}}>
               Submit
              </Button>
            </FormGroup>
          </FormControl>
        </form>
      </>
    )
  }
}
export default Email;