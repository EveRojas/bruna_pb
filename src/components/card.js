import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Profile2 from "../assets/profile2.jpg"
import Brand from "../assets/brand.jpg";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    boxShadow:'0px 0px 3px 1px #AAA', 
    borderRadius: '10px',  
    width: '100%', 
    height: '100%',
     },
  media: {
    height: 300,
  },
});

export default function MediaCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={Profile2}
          title="Chef Bruna Grimaldi"
          href="https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20"
        />
        <CardContent>
          <Typography gutterBottom style={{font:'400 18px system-ui', paddingBottom:'0.5rem'}}>
              Vamos trabalhar juntos?
          </Typography>
          <Typography gutterBottom style={{font:'400 13px system-ui'}}>
            Consultoria <img src={Brand} style={{width:'13px', height:'13px', marginTop:'1.2rem', paddingRight:'0.1rem'}}/>
 Eventos <img src={Brand} style={{width:'13px', height:'13px', marginTop:'1.2rem', paddingRight:'0.1rem'}}/> 
 Classes <img src={Brand} style={{width:'13px', height:'13px', marginTop:'1.2rem', paddingRight:'0.1rem'}}/> 
 Private Chef
          </Typography>
          <Typography gutterBottom color="textSecondary" style={{font:'400 10px system-ui', paddingTop:'0.5rem', paddingBottom:'0.5rem'}}>
            brunagrimaldi@hotmail.com
          </Typography>
          <Typography variant="body2" color="textSecondary" style={{font:'400 10px system-ui'}}>
            +55 81 97327 7360 (somente ligações)
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="secondary" style={{font:'400 13px system-ui'}} href="https://api.whatsapp.com/send?phone=447379827042&text=Ol%C3%A1!%20Tudo%20bem?%20Eu%20sou%20a%20Chef%20Bruna%20Grimaldi.%20Como%20posso%20te%20ajudar?%20">
          WHATSAPP
        </Button>
        <Button size="small" color="secondary" href="aboutme" style={{font:'400 13px system-ui'}}>
          Saiba Mais
        </Button>
      </CardActions>
    </Card>
  );
}