// shared/routes.js
import Home from './components/home'
import AboutMe from './components/aboutme'
import Follow from './components/follow'
import Contact from './components/contact'
import NotFound from './components/NotFound';

const routes =  [
  {
    path: '/',
    exact: true,
    component: Home,
  },
    {
    path: '/aboutme',
    component: AboutMe,
  },
    {
    path: '/follow',
    component: Follow,
  },
    {
    path: '/contact',
    component: Contact,
  },
  {
    component: NotFound
  }
];

export default routes;