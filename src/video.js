import React from "react";
import Video from "./headervideo.mp4";

const Video = () => {
  return (
    <div className="container">
      <video autoPlay playsInline muted src={Video} />
    </div>
  );
};

export default Video;