import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router,
         Switch,
         Route,
         Link} from "react-router-dom"; 
import "./styles.css";
import { makeStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import SideBar from "./sidebar";
import Home from "./components/home";
import AboutMe from "./components/aboutme";
import KnowMore from "./components/knowmore";
import Follow from "./components/follow";
import FollowEn from "./components/followEn";
import Contact from "./components/contact";
import ContactEn from "./components/contactEn";
import NotFound from "./components/NotFound";
import SocialFollow from "./components/socialfollow";
import Brand from "./assets/brand.jpg";
import Routes from "./routes"

function App() {


  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div id="App">
      <SideBar pageWrapId={"page-wrap"} outerContainerId={"App"} />

      <div id="page-wrap-brand" className='sticky'>
        <div id='header'>    
        <h1>Bruna Grimaldi</h1>
        <img src={Brand} style={{width:'20px', height:'20px', marginTop:'1.2rem'}}/>
        </div>
        <SocialFollow/>

      </div>
  <main>
    <Switch>
      <Router>
          <Route exact path="/"      component={Home} />
          <Route path="/aboutme" component={AboutMe} />
          <Route path="/knowmore" component={KnowMore} />
          <Route path="/follow" component={Follow} />
          <Route path="/followEn" component={FollowEn} />
          <Route path="/contact" component={Contact} />
          <Route path="/contactEn" component={ContactEn} />
          <Route component={NotFound} />
      </Router>
    </Switch>
  </main>
  <footer className='footer'>
 <div style={{ left: '0', paddingTop: '0.5rem', bottom: '0'}}>
  <Button style={{font:'300 10px system-ui', color:'darkgrey'}} href="contactEn" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
    Contact
      </Button>
            <Button style={{font:'300 10px system-ui', color:'darkgrey'}} href="/" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
    Home
      </Button>

      <Button style={{font:'300 10px system-ui', color:'darkgrey'}} href="knowmore" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
    Know more
      </Button>
 </div>
      <h5>copyright 2020@EveRojas</h5>
 
  </footer>

    </div>
  );
}

export default App;
